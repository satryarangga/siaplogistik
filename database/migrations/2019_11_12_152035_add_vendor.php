<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pok', function (Blueprint $table) {
            $table->string('nama_paket', 100)->nullable();
            $table->string('penyedia', 100)->nullable();
            $table->string('nilai_penawaran', 100)->nullable();
            $table->string('pejabat_pengadaan', 100)->nullable();
            $table->string('pejabat_komitmen', 100)->nullable();
            $table->string('nilai_kontrak', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pok', function (Blueprint $table) {
            $table->dropColumn('nama_paket');
            $table->dropColumn('penyedia');
            $table->dropColumn('nilai_penawaran');
            $table->dropColumn('pejabat_pengadaan');
            $table->dropColumn('pejabat_komitmen');
            $table->dropColumn('nilai_kontrak');
        });
    }
}
