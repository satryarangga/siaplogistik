<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePok extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pok', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_program')->nullable();
            $table->string('program')->nullable();
            $table->string('kode_kegiatan')->nullable();
            $table->string('kegiatan')->nullable();
            $table->string('komponen')->nullable();
            $table->string('akun')->nullable();
            $table->string('kode_akun')->nullable();
            $table->string('detail')->nullable();
            $table->string('sub_detail')->nullable();
            $table->string('volume')->nullable();
            $table->string('harga_satuan')->nullable();
            $table->string('satuan')->nullable();
            $table->string('jumlah')->nullable();
            $table->string('jenis_belanja')->nullable();
            $table->string('metode_pelaksanaan')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pok');
    }
}
