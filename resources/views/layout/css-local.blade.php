@if(isset($localAsset))
	@foreach($localAsset as $key => $val)
		@includeIf('util.css.'.$val)
	@endforeach
@endif

@includeIf($local_css_path.$page)