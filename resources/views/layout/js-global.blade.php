<!-- jQuery 3 -->
<script src="{{asset('lte/bower_components')}}/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('lte/bower_components')}}/jquery-ui/jquery-ui.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte/bower_components')}}/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="{{asset('lte')}}/js/adminlte.min.js"></script>

<script src="{{ asset('lte') }}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('lte') }}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('lte') }}//js/custom-file-input.js"></script>

<script>
  $(function () {
    $('#example1').DataTable({
    	"order":[[0, 'desc']]
    })
  })
</script>

@include('layout.js-local')