<header class="main-header" style="background-color:#fff">
    <!-- Logo -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        `<img src="{{asset('lte/img')}}/logo.jpeg" style="width:45px" />  
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
      `<img src="{{asset('lte/img')}}/logo.jpeg" style="width:45px" />
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <h4 style="float:left;margin-top:15px">Sistem Informasi Administrasi Pengadaan</h4>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can bepside found in dropdown.less -->
          <li class="dropdown user user-menu" style="background-color:#dd4b39">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{$user['name']}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{route('user.edit', ['id' => Auth::id()])}}?type=profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>