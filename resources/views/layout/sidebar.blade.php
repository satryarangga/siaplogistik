<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
          <li @if($page == 'user') class="active" @endif>
            <a href="{{route('user.index')}}">
            <i class="fa fa-users"></i> <span>Home</span>
            </a>
          </li>

          <li @if($page == 'pok') class="active" @endif>
            <a href="{{route('pok.index')}}">
            <i class="fa fa-newspaper-o"></i> <span>List POK</span>
            </a>
          </li>

          <li @if($page == 'pok') class="active" @endif>
            <a href="{{route('pok.create')}}">
            <i class="fa fa-newspaper-o"></i> <span>POK</span>
            </a>
          </li>

          <li @if($page == 'pl') class="active" @endif>
            <a href="{{route('pel')}}">
            <i class="fa fa-newspaper-o"></i> <span>PL</span>
            </a>
          </li>

          <li @if($page == 'pal') class="active" @endif>
            <a href="{{route('pal')}}">
            <i class="fa fa-newspaper-o"></i> <span>PAL</span>
            </a>
          </li>

          <li @if($page == 'penyedia') class="active" @endif>
            <a href="{{route('penyedia.index')}}">
            <i class="fa fa-newspaper-o"></i> <span>Daftar Penyedia</span>
            </a>
          </li>
          
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>