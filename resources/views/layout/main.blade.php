<!DOCTYPE html>
<html>
  <head>

    <!-- META -->
    @include("layout.meta")

    <!-- CSS -->
    @include("layout.css-global")

  </head>
  <body class="hold-transition skin-white sidebar-mini">
    <div class="wrapper">

      <!-- HEADER -->
      @include("layout.header")

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- SIDEBAR -->
        @include("layout.sidebar")    

        @yield("content")
        <div style="clear: both;"></div>
      
      </div>

      <!-- SIDEBAR -->
      @include("layout.footer")    

    </div>

    <!-- SIDEBAR -->
    @include("layout.js-global")      

  </body>
</html>
