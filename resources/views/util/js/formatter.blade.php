<script type="text/javascript">
	function moneyFormatter(elem) {
        var n = parseInt(elem.val().replace(/\D/g, ''), 10);

        if (isNaN(n)) {
            elem.val('0');
        } else {
            elem.val(n.toLocaleString());
        }
    }

    function toMoney(num) {
        return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

    function toInt(money) {
         return Number(money.replace(/[^0-9\.-]+/g,""));
    }

    function toDecimal(num) {
        return Math.round(num * 100) / 100;
    }
    
</script>