@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
								<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Nama Penyedia</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="nama" value="{{$row->nama}}" id="nama" placeholder="Nama">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Rekening</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="rekening" value="{{$row->rekening}}" id="rekening" placeholder="Rekening">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Alamat</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="alamat" value="{{$row->alamat}}" id="alamat" placeholder="Alamat">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">NPWP</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="npwp" value="{{$row->npwp}}" id="npwp" placeholder="NPWP">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Nama Pejabat</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="pejabat" value="{{$row->pejabat}}" id="pejabat" placeholder="Nama Pejabat">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-danger pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection