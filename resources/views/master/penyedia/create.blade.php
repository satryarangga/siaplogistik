@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Create New {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
	            <form class="form-horizontal" action="{{route("$page.store")}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Nama Penyedia</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="nama" value="{{old('nama')}}" id="nama" placeholder="Nama Penyedia">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Rekening</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="rekening" value="{{old('rekening')}}" id="rekening" placeholder="Rekening">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Alamat</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="alamat" value="{{old('alamat')}}" id="alamat" placeholder="Alamat">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">NPWP</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="npwp" value="{{old('npwp')}}" id="npwp" placeholder="NPWP">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Nama Pejabat</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="pejabat" value="{{old('pejabat')}}" id="pejabat" placeholder="Pejabat">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-danger pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	            </form>
	          </div>
          </div>
    </section>

@endsection