@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
	<div class="col-md-12">
		{!! session('displayMessage') !!}
		<div class="box">
            <div class="box-header">
                <a href="{{route($page.'.create')}}" class="btn btn-danger">Tambah {{ucwords(str_replace('-',' ', $page))}}</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example-1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Rekening</th>
                  <th>NPWP</th>
                  <th>Nama Pejabat</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                <td style="width:10%">{{$key + 1}}</td>
                <td style="width:20%">{{$val->nama}}</td>
                <td style="width:20%">{{$val->alamat}}</td>
                <td style="width:20%">{{$val->rekening}}</td>
                <td style="width:20%">{{$val->npwp}}</td>
                <td style="width:20%">{{$val->pejabat}}</td>
                <td style="width:2%">
                  <a href="{{ route($page.'.edit', ['id' => $val->id]) }}">
                    <i class="fa fa-pencil" style="background-color:#fff;color:#dd4b39"></i>
                  </a>
                  <form class="deleteForm" method="post" action="{{route("$page.destroy", ['id' => $val->id])}}">
                    {{csrf_field()}}
                      <button style="padding:0" onclick="return confirm('You will delete this {{$page}}, continue')" type="submit">
                        <i class="fa fa-trash" style="background-color:#dd4b39;color:#fff"></i>
                      </button>
                      {{ method_field('DELETE') }}
                    </form>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection