@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
  <div class="col-md-2">
    <div class="box">
      <div class="box-header">
        <h5>Filter</h5>
        <form>
          <div class="form-group">
            <label for="email">Metode Pelaksanaan</label>
            <select class="form-control" name="filter-metode">
              <option value="">Semua</option>
              <option @if(isset($filter['filter-metode']) && $filter['filter-metode'] == 'PAL') selected @endif value="PAL">Pengadaan Langsung</option>
              <option @if(isset($filter['filter-metode']) && $filter['filter-metode'] == 'PL') selected @endif value="PL">Penunjukkan Langsung</option>
            </select>
          </div>
          <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="text" value="{{(isset($filter['filter-tahun'])) ? $filter['filter-tahun'] : ''}}" class="form-control" id="tahun" name="filter-tahun">
          </div>
          <button type="submit" class="btn btn-default">Search</button>
      </form>
      </div>
      <div class="box-body">

      </div>
    </div>
  </div>
	<div class="col-md-10">
		{!! session('displayMessage') !!}
		<div class="box">
            <div class="box-header">
                <a href="{{route($page.'.create')}}" class="btn btn-danger">Create {{ucwords(str_replace('-',' ', $page))}}</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kegiatan</th>
                  <th>Komponen</th>
                  <th>Akun</th>
                  <th>Detail</th>
                  <th>Sub Detail</th>
                  <th>Jumlah</th>
                  <th>Metode Pelaksanaan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                <td style="width:10%">{{$key + 1}}</td>
                <td style="width:20%">{{$val->kegiatan}}</td>
                <td style="width:20%">{{$val->komponen}}</td>
                <td style="width:20%">{{$val->akun}}</td>
                <td style="width:10%">{{$val->detail}}</td>
                <td style="width:10%">{{$val->sub_detail}}</td>
                <td style="width:15%">{{$val->jumlah}}</td>
                <td style="width:5%">{{$val->metode_pelaksanaan}}</td>
                <td style="width:2%">
                  <a href="{{ route($page.'.edit', ['id' => $val->id]) }}">
                    <i class="fa fa-pencil" style="background-color:#fff;color:#dd4b39"></i>
                  </a>
                  <form class="deleteForm" method="post" action="{{route("$page.destroy", ['id' => $val->id])}}">
                    {{csrf_field()}}
                      <button style="padding:0" onclick="return confirm('You will delete this {{$page}}, continue')" type="submit">
                        <i class="fa fa-trash" style="background-color:#dd4b39;color:#fff"></i>
                      </button>
                      {{ method_field('DELETE') }}
                    </form>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            {{ $result->links()}}
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection