<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
        }
        #wrapper {
        background-color: green; /* for visualization purposes */
        text-align: center;
        }
        #yourdiv {
        background-color: red; /* for visualization purposes */
        display: inline-block;
        }
	</style>
        <center>
            <h5 style="font-size:14px">BADAN INTELIJEN NEGARA</h5>
            <h5 style="font-size:14px">UNIT KERJA PENGADAAN BARANG / JASA</h5>
        </center>
        <div style="margin-top:20px;">
            <center>
                <h5 style="text-decoration:underline;font-size:14px;">BERITA ACARA PENELITIAN HARGA</h5>
                <h6>Nomor : {{$pok->kode_program}}</h6>
                <h6 style="margin-top:10px;margin-bottom:10px">Tentang</h6>
                <h6 style="width:50%;text-align:center;margin:0 auto;font-size:14px">{{$pok->kegiatan}}</h6>
            </center>
        </div>

        <div style="text-align:center">
            <p style="font-size:11px;margin-top:20px;text-align:justify">Yang bertanda tangan dibawah ini Pejabat Pengadaan telah menerima Penawaran Harga {{$pok->program}}  di kantor BIN Jakarta  yang diajukan oleh Perusahaan :</p>
        </div>

        <div style="margin-top:20px;">
            <center>
            <table style="margin:0 auto;">
                <tr>
                    <td>Nama Perusahaan</td>
                    <td>:</td>
                    <td>{{$penyedia->nama}}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{$penyedia->alamat}}</td>
                </tr>
                <tr>
                    <td>Nilai Penawaran</td>
                    <td>:</td>
                    <td>{{moneyFormat($pok->nilai_penawaran)}}</td>
                </tr>
            </table>
            </center>
        </div>
        <div style="margin-top:20px">
            <center>
            <p style="font-size:11px;text-align:justify">Setelah  meneliti  dan  memeriksa  penawaran harga tersebut di atas, telah memenuhi persyaratan yang ditentukan, dan dapat diterima untuk melaksanakan pekerjaan.</p>
            </center>
        </div>

        <div style="margin-top:20px">
            <center>
            <p style="font-size:11px;text-align:justify">	Demikian Berita Acara Penelitian Harga ini dibuat dengan sesungguhnya untuk dapat dipergunakan sebagaimana mestinya.</p>
            </center>
        </div>

        <div style="margin-top:80px;margin-right:20px;float:right">
            <center>
                <label style="font-size:14px">Jakarta {{date('j F Y')}}</label><br />
                <label style="font-weight:600;font-size:14px">a.n. Kepala badan Intelijen Negara</label><br />
                <label style="font-weight:600;font-size:14px">Pejabat Pengadaan</label><br />
                <div style="margin-top:100px">
                <label style="font-weight:600;font-size:14px">{{$pok->pejabat_pengadaan}}</label><br />
                </div>
            </center>
        </div>
</body>
</html>