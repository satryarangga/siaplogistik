@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			{!! session('displayMessage') !!}
			<div class="box box-info">
	            <div class="box-header">
	              <h3 class="box-title">Buat {{ucwords(str_replace('-',' ', $page))}}</h3>
								<form method="post" action="{{route('import')}}" enctype="multipart/form-data">
									@csrf
									<div class="box">
									<input style="visibility:hidden" type="file" name="pok" accept=".xlsx" id="file-1" class="inputfile inputfile-1" data-multiple-caption="files selected" />
									<label style="background-color:#dd4b39;border-radius:3px" for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> 
									<span>Pilih File POK</span></label>
										<input type="submit" value="Upload" class="btn btn-danger" style="margin-top:-30px" />
										<a style="margin-top:-30px" href="{{asset('FormatPOK.xlsx')}}" class="btn btn-danger" download>Download Format POK</a>
									</div>
								</form>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
	            <form class="form-horizontal" action="{{route("$page.store")}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Program</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_program" value="{{old('kode_program')}}" id="kode_program">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Program</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="program" value="{{old('program')}}" id="program">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Metode</label>
	                  <div class="col-sm-8">
	                    <select name="metode_pelaksanaan" class="form-control">
												<option value="PL">Penunjukan Langsung</option>
												<option value="PAL">Pengadaan Langsung</option>
											</select>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Jenis Belanja</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="jenis_belanja" value="{{old('jenis_belanja')}}" id="jenis_belanja">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Kegiatan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_kegiatan" value="{{old('kode_kegiatan')}}" id="kode_kegiatan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kegiatan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kegiatan" value="{{old('kegiatan')}}" id="kegiatan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Komponen</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="komponen" value="{{old('komponen')}}" id="komponen">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Akun</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_akun" value="{{old('kode_akun')}}" id="kode_akun">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Akun</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="akun" value="{{old('akun')}}" id="akun">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Detail</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="detail" value="{{old('detail')}}" id="detail">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Sub Detail</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="sub_detail" value="{{old('sub_detail')}}" id="sub_detail">
	                  </div>
	                </div>
									

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Jumlah</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="jumlah" value="{{old('jumlah')}}" id="jumlah">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Volume</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="volume" value="{{old('volume')}}" id="volume">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Satuan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="satuan" value="{{old('satuan')}}" id="satuan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Harga Satuan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="harga_satuan" value="{{old('harga_satuan')}}" id="harga_satuan">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-danger pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	            </form>
	          </div>
          </div>
    </section>

@endsection