@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
  <div class="col-md-2">
    <div class="box">
      <div class="box-header">
        <h5>Filter</h5>
        <form>
          <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="text" value="{{(isset($filter['filter-tahun'])) ? $filter['filter-tahun'] : ''}}" class="form-control" id="tahun" name="filter-tahun">
          </div>
          <button type="submit" class="btn btn-default">Search</button>
      </form>
      </div>
      <div class="box-body">

      </div>
    </div>
  </div>
	<div class="col-md-10">
		{!! session('displayMessage') !!}
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>Kode Program</th>
                  <th>Akun</th>
                  <th>Detail</th>
                  <th>Metode Pelaksanaan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                <td>{{$val->kode_program}}</td>
                <td>{{$val->akun}}</td>
                <td>{{$val->sub_detail}}</td>
                <td>{{$val->metode_pelaksanaan}}</td>
                <td>
                  <a class="btn btn-danger" href="{{ route($page.'.edit', ['id' => $val->id]) }}?type=pal">Detail</a>
                  <a class="btn btn-danger" href="{{ route('doc', ['id' => $val->id]) }}">Lihat Dokumen</a>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection