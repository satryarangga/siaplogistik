@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
	<div class="col-md-12">
		{!! session('displayMessage') !!}
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Dokumen</th>
                  <th>Nomor Surat</th>
                  <th>Tanggal Surat</th>
                  <th>Input Nomor Surat</th>
                  <th>Generate</th>
                  <th>Download</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                  <td>{{$key + 1}}</td>
                  <td>{{$val}}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><a href="{{route('pdf', ['id' => $row->id])}}" class="btn btn-danger">Download Dokumen</a></td>
                  <!-- <td><a href="{{route('preview', ['id' => $row->id])}}" class="btn btn-danger">View Dokumen</a></td> -->
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection