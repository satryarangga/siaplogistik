@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                
								<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Program</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_program" value="{{$row->kode_program}}" id="kode_program">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Program</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="program" value="{{$row->program}}" id="program">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Metode</label>
	                  <div class="col-sm-8">
	                    <select name="metode_pelaksanaan" class="form-control">
												<option @if($row->metode_pelaksanaan == 'PL') selected @endif value="PL">Penunjukan Langsung</option>
												<option @if($row->metode_pelaksanaan == 'PAL') selected @endif value="PAL">Pengadaan Langsung</option>
											</select>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Jenis Belanja</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="jenis_belanja" value="{{$row->jenis_belanja}}" id="jenis_belanja">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Kegiatan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_kegiatan" value="{{$row->kode_kegiatan}}" id="kode_kegiatan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kegiatan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kegiatan" value="{{$row->kegiatan}}" id="kegiatan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Komponen</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="komponen" value="{{$row->komponen}}" id="komponen">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Kode Akun</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="kode_akun" value="{{$row->kode_akun}}" id="kode_akun">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Akun</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="akun" value="{{$row->akun}}" id="akun">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Detail</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="detail" value="{{$row->detail}}" id="detail">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Sub Detail</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="sub_detail" value="{{$row->sub_detail}}" id="sub_detail">
	                  </div>
	                </div>
									

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Jumlah</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="jumlah" value="{{$row->jumlah}}" id="jumlah">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Volume</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="volume" value="{{$row->volume}}" id="volume">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Satuan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="satuan" value="{{$row->satuan}}" id="satuan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Harga Satuan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="harga_satuan" value="{{$row->harga_satuan}}" id="harga_satuan">
	                  </div>
	                </div>

									@if($type == 'pal' || $type == 'pl')
									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Nama Paket</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="nama_paket" value="{{$row->nama_paket}}" id="nama_paket">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Nama Penyedia</label>
	                  <div class="col-sm-8">
	                    <select name="penyedia" class="form-control">
												<option>Pilih Penyedia</option>
												@foreach($penyedia as $key => $val)
												<option @if($row->penyedia == $val->id) selected @endif value="{{$val->id}}">{{$val->nama}}</option>
												@endforeach
											</select>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Nilai Penawaran</label>
	                  <div class="col-sm-8">
	                    <input type="number" class="form-control" name="nilai_penawaran" value="{{$row->nilai_penawaran}}" id="nilai_penawaram">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Pejabat Pengadaan</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="pejabat_pengadaan" value="{{$row->pejabat_pengadaan}}" id="pejabat_pengadaan">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Pejabat Komitmen</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="pejabat_komitmen" value="{{$row->pejabat_komitmen}}" id="pejabat_komitmen">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Nilai Kontrak</label>
	                  <div class="col-sm-8">
	                    <input type="number" class="form-control" name="nilai_kontrak" value="{{$row->nilai_kontrak}}" id="nilai_kontrak">
	                  </div>
	                </div>

									<input type="hidden" name="type" value="{{$type}}" />
									@endif
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-danger pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection