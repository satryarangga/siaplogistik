@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
	<div class="col-md-12">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                  <img src="{{asset('slider-siap')}}/1.png" alt="First slide">
                </div>
                @for($i=2;$i<=53;$i++)
                <div class="item">
                  <img src="{{asset('slider-siap')}}/{{$i}}.png" alt="First slide">
                </div>
                @endfor
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="fa fa-angle-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="fa fa-angle-right"></span>
              </a>
            </div>
    </div>
	</div>
</section>

@endsection