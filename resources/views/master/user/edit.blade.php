@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Username</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" value="{{$row->username}}" readonly >
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Name</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="name" value="{{$row->name}}" id="name" placeholder="Last Name">
	                  </div>
	                </div>

	                @if(Auth::id() == $row->id)
	                <div class="form-group">
	                  <label for="file" class="col-sm-4 control-label">Password *Fill password if you want to change</label>
	                  <div class="col-sm-8">
	                    <input type="password" class="form-control" name="password" id="pass">
	                    <input type="hidden" name="type" value="{{$type}}" />
	                  </div>
	                </div>
	                @endif

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-danger pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection