<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penyedia extends Model
{
    /**
     * @var string
     */
    protected $table = 'penyedia';

    /**
     * @var array
     */
    protected $fillable = [
        'nama', 'alamat', 'npwp', 'pejabat', 'rekening'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

}
