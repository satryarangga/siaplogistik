<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pok extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'pok';

    /**
     * @var array
     */
    protected $fillable = [
        'kode_program', 'program', 'kode_kegiatan', 'metode_pelaksanaan', 'jenis_belanja', 
        'kegiatan', 'komponen', 'akun', 'sub_detail', 'kode_akun', 'detail', 'harga_satuan',
        'jumlah', 'volume', 'satuan','created_by', 'updated_by', 'nama_paket', 'penyedia',
        'nilai_penawaran', 'pejabat_pengadaan', 'pejabat_komitmen', 'nilai_kontrak'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public static function list($filter) {
        $where = [];
        $year = date('Y');

        if(isset($filter['filter-metode'])) {
            $where[] = ['metode_pelaksanaan', '=', $filter['filter-metode']];
        }

        if(isset($filter['filter-tahun'])) {
            $year = $filter['filter-tahun'];
        }

        return parent::where($where)
                        ->whereYear('created_at', $year)
                        ->paginate(20);
    }
}
