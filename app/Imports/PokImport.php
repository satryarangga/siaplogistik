<?php

namespace App\Imports;

use App\Models\Pok;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PokImport implements ToModel,WithStartRow
{
    public function startRow(): int
    {
        return 2;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Pok([
            'kode_program' => $row[0],
            'program' => $row[1],
            'kode_kegiatan' => $row[2],
            'kegiatan' => $row[3],
            'komponen' => $row[4],
            'akun' => $row[5],
            'kode_akun' => $row[6],
            'detail' => $row[7],
            'sub_detail' => $row[8],
            'volume' => $row[9],
            'harga_satuan' => $row[10],
            'satuan' => $row[11],
            'jumlah' => $row[12],
            'jenis_belanja' => $row[13],
            'metode_pelaksanaan' => $row[14],
            'created_by' => 1,
        ]);
    }
}
