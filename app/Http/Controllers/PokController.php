<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\PokImport;
use App\Models\Pok;
use App\Models\Penyedia;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class PokController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'pok';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [
            'result' => Pok::list($request->input()),
            'page' => $this->page,
            'module' => $this->module,
            'filter' => $request->input()
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penyedia = Penyedia::all();
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'penyedia' => $penyedia
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create = [
            'kode_program'  => $request->input('kode_program'),
            'program'  => $request->input('program'),
            'metode_pelaksanaan'  => $request->input('metode_pelaksanaan'),
            'jenis_belanja'  => $request->input('jenis_belanja'),
            'kode_kegiatan'  => $request->input('kode_kegiatan'),
            'kegiatan'  => $request->input('kegiatan'),
            'komponen'  => $request->input('komponen'),
            'kode_akun'  => $request->input('kode_akun'),
            'akun'  => $request->input('akun'),
            'sub_detail'  => $request->input('sub_detail'),
            'detail'  => $request->input('detail'),
            'jumlah'  => $request->input('jumlah'),
            'volume'  => $request->input('volume'),
            'satuan'  => $request->input('satuan'),
            'harga'  => $request->input('harga'),
            'harga_satuan'  => $request->input('harga_satuan'),
            'created_by' => Auth::id()
        ];

        $genre = Pok::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $penyedia = Penyedia::all();
        $type = $request->input('type');
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'penyedia' => $penyedia,
            'row' => Pok::find($id),
            'type' => $type
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Pok::find($id);
        $type = $request->input('type');

        $update = [
            'kode_program'  => $request->input('kode_program'),
            'program'  => $request->input('program'),
            'metode_pelaksanaan'  => $request->input('metode_pelaksanaan'),
            'jenis_belanja'  => $request->input('jenis_belanja'),
            'kode_kegiatan'  => $request->input('kode_kegiatan'),
            'kegiatan'  => $request->input('kegiatan'),
            'komponen'  => $request->input('komponen'),
            'kode_akun'  => $request->input('kode_akun'),
            'akun'  => $request->input('akun'),
            'sub_detail'  => $request->input('sub_detail'),
            'detail'  => $request->input('detail'),
            'jumlah'  => $request->input('jumlah'),
            'volume'  => $request->input('volume'),
            'satuan'  => $request->input('satuan'),
            'harga'  => $request->input('harga'),
            'harga_satuan'  => $request->input('harga_satuan'),
            'nama_paket'  => $request->input('nama_paket'),
            'penyedia'  => $request->input('penyedia'),
            'nilai_penawaran'  => $request->input('nilai_penawaran'),
            'pejabat_pengadaan'  => $request->input('pejabat_pengadaan'),
            'pejabat_komitmen'  => $request->input('pejabat_komitmen'),
            'nilai_kontrak'  => $request->input('nilai_kontrak'),
            'updated_by' => Auth::id()
        ];

        $data->update($update);

        if($type == 'pl') {
            $url = route('pel');
        } else if($type == 'pal') {
            $url = route('pal');
        } else {
            $url = route($this->page.'.index');
        }
        $message = setDisplayMessage('success', "Success to update ".$this->page);
        return redirect($url)->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pok::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function pal(Request $request) {
        $data = [
            'result' => Pok::where('metode_pelaksanaan', 'PAL')->get(),
            'page' => $this->page,
            'module' => $this->module,
            'filter' => $request->input()
        ];
        
        return view($this->module . '/' . $this->page . ".pal", $data);

    }

    public function pel(Request $request) {
        $data = [
            'result' => Pok::where('metode_pelaksanaan', 'PL')->get(),
            'page' => $this->page,
            'module' => $this->module,
            'filter' => $request->input()
        ];
        
        return view($this->module . '/' . $this->page . ".pel", $data);
    }

    public function import(Request $request) {
        if(!request()->file('pok')) {
            $message = setDisplayMessage('danger', "Pilih file terlebih dahulu");
            return redirect(route($this->page.'.create'))->with('displayMessage', $message);
        }
        Excel::import(new PokImport,request()->file('pok'));
           
        $message = setDisplayMessage('success', "Success to upload ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function doc($id) {
        $row = Pok::find($id);

        if($row->metode_pelaksanaan == "PL") {
            $result = [
                'SBD',
                'JUKSUNG',
                'KEP',
                'SP',
                'SSKK',
                'SSUK',
                'SPB',
                'BAPLK',
                'SPMK',
                'BAST',
                'BAP',
                'Lampiran'
            ];
        } else {
            $result = [
                'Dokumen Permintaan',
                'BAPH',
                'BANH',
                'BAHPL',
                'Pengunguman',
                'Keputusan',
                'SPK',
                'SPB',
                'BAST',
                'BAP'
            ];
        }

        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Pok::find($id),
            'result' => $result
        ];

        return view($this->module . '/' . $this->page . ".doc", $data);        
    }

    public function cetakPdf($id)
    {
    	$pok = Pok::find($id);
        $penyedia = Penyedia::find($pok->penyedia);
    	$pdf = PDF::loadview($this->module . '/' . $this->page . ".pdf",['pok'=>$pok, 'penyedia' => $penyedia]);
    	return $pdf->download($pok->kode_program);
    }

    public function previewPdf($id)
    {
        $pok = Pok::find($id);
        $penyedia = Penyedia::find($pok->penyedia);
        $data = [
            'pok' => $pok,
            'penyedia' => $penyedia
        ];
    	return view($this->module . '/' . $this->page . ".pdf", $data);   
    }
}
