<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Penyedia;


class PenyediaController extends Controller
{
    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;


    public function __construct() {
        $this->module = 'master';
        $this->page = 'penyedia';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $data = [
            'result' => Penyedia::all(),
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page' => $this->page,
            'module' => $this->module
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'     => 'required',
            'alamat'     => 'required',
            'npwp'     => 'required',
            'rekening'     => 'required',
            'pejabat'     => 'required'
        ]);

        $create = [
            'nama'  => $request->input('nama'),
            'alamat'  => $request->input('alamat'),
            'npwp'  => $request->input('npwp'),
            'rekening'  => $request->input('rekening'),
            'pejabat'  => $request->input('pejabat')
        ];

        $user = Penyedia::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $type = $request->input('type');
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Penyedia::find($id),
            'type' => $type
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama'     => 'required',
            'alamat'     => 'required',
            'npwp'     => 'required',
            'rekening'     => 'required',
            'pejabat'     => 'required'
        ]);

        $data = Penyedia::find($id);

        $update = [
            'nama'  => $request->input('nama'),
            'alamat'  => $request->input('alamat'),
            'npwp'  => $request->input('npwp'),
            'rekening'  => $request->input('rekening'),
            'pejabat'  => $request->input('pejabat')
        ];

        $data->update($update);

        $message = setDisplayMessage('success', "Success to update ".$this->page);

        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Penyedia::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }
}
