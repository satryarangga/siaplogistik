<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'UserController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('user', 'UserController');
Route::get('pal', 'PokController@pal')->name('pal');
Route::get('pl', 'PokController@pel')->name('pel');
Route::get('doc/{id}', 'PokController@doc')->name('doc');
Route::get('cetak-pdf/{id}', 'PokController@cetakPdf')->name('pdf');
Route::get('preview-pdf/{id}', 'PokController@previewPdf')->name('preview');
Route::post('import', 'PokController@import')->name('import');
Route::resource('pok', 'PokController');
Route::resource('penyedia', 'PenyediaController');